package com.ci.myShop.model;

public class Paper extends Consumable {
	private String quality;
	private Float weight ;
	

	public Paper(String name, int id, float price, int nbrElt, int quantity, String quality, Float weight) {
		super(name, id, price, nbrElt, quantity);
		this.quality = quality;
		this.weight = weight;
	}


	public String getQuality() {
		return quality;
	}


	public void setQuality(String quality) {
		this.quality = quality;
	}


	public Float getWeight() {
		return weight;
	}


	public void setWeight(Float weight) {
		this.weight = weight;
	}


	

}
