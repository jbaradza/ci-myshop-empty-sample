package com.ci.myShop.controller;

import java.util.ArrayList;

import com.ci.myShop.model.Book;
import com.ci.myShop.model.Item;

public class Shop {

	private static Storage storage;
	private static float cash;

	public Shop(Storage storage, float cash) {
		super();
		this.storage = storage;
		this.cash = cash;
	}

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

	public static float getCash() {
		return cash;
	}

	public static  void setCash(float cashy) {
		cash = cashy;
	}

	public static Item sell(String Name) {
		Item item = null;

		if (storage.getItem(Name) == null) {
			System.out.println("il n'existe pas");

		} else {
			System.out.println("oui pret � vendre");
			item = (Item) storage.getItem(Name);
			setCash(getCash()+item.getPrice());
			int i = storage.getItemMap().indexOf(item);
			storage.getItemMap().remove(i);
		}

		return item;
	}

	public boolean buy(Item item) {
		boolean bool = false;
		if (item.getPrice() <= getCash()) {
			setCash(getCash()-item.getPrice());
			System.out.println("Tu  peux acheter");
			bool = true;
		} else {
			System.out.println("Tu ne peux pas acheter");
		}
		return bool;
	}

	public boolean isItemAvailable(String name) {
		boolean lol = false;
		if (storage.getItem(name) != null) {
			lol = true;
		}

		return lol;
	}

	public int getAgeForBook(String name) {
		Book book =  (Book) storage.getItem(name);
		int age = 0;
		if (book != null && book instanceof Book) {		
				 age = book.getAge();	
		}
		return age;

	}
}
