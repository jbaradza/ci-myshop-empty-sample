package com.ci.myShop.controller;

import java.util.ArrayList;
import java.util.List;


import com.ci.myShop.model.Item;


public class Storage<T extends Item,Book,Paper,Pen,Consumable> {

	private static  List itemMap ;


	public  Storage() {
		itemMap = new ArrayList<T>();
	}

	public  List<T> getItemMap() {
		return itemMap;
	}

	public  void addItem(T obj) {
		itemMap.add(obj);
	}
	
	public T getItem( String name) {
		T item = null;
		if(getItemMap().size() != 0) {
	    for(T itm : (ArrayList<T>)getItemMap()) {
			if(  itm.getName() == name ) {
				return item = itm;
			}
		  }
	     }else {
	    	 System.out.println("No item");
	     }
		
    return item;
}
	
}
