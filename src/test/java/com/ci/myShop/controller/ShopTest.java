package com.ci.myShop.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.ci.myShop.model.Item;

public class ShopTest {
	public Item item2 = null;
	public Item item1 = null;
	public Item item3 = null;
	public Shop sho = null;

	@Before
	public void ShopItems() {
		item1 = new Item("book1", 10, 110f, 20);
		item2 = new Item("book2", 102, 110f, 208);
		item3 = new Item("book3", 1040, 1100f, 208);

		Storage storage = new Storage();
		storage.addItem(item1);
		storage.addItem(item2);
		storage.addItem(item3);
		sho = new Shop(storage, 100f);

	}

	@Test
	public void ItemSell() {

		assertEquals(sho.sell("book2"), item2);
	}

	@Test
	public void testCashBalanceAfterSell() {
		sho.sell("book2");
		assertEquals(210f, sho.getCash(), 0.0f);
	}

	@Test
	public void testCashBalanceAfterBuy() {
		sho.buy(item2);
		assertEquals(100f, sho.getCash(), 0.0f);
	}
	
	@Test
	public void testItemAvailable() {
		
		assertEquals(true,sho.isItemAvailable("book3"));
	}
	
}
