package com.ci.myShop.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.ci.myShop.model.Item;

public class StorageTest {
	
	@Test
	   public void ItemExist() {
		Item item2 = new Item("book", 52, 36, 4578);
		Storage sto = new Storage();
		sto.addItem(item2);
		    assertEquals(sto.getItem("book"), item2);
	}
	
	
	
}

